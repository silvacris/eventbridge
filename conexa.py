import json

def registerConexa(event, context):
    print(event)

    body = {
        "message": "Executing register conexa lambda!",
        "input": event,
    }

    print(body)

    response = {"statusCode": 200, "body": body}

    return response

def activateConexa(event, context):
    print(event)

    body = {
        "message": "Executing activate conexa lambda!",
        "input": event,
    }

    print(body)

    response = {"statusCode": 200, "body": body}

    return response